package com.example.tasks;

import com.example.ui.Log;
import com.example.ui.Meet;
import com.example.ui.MenuAdmin;
import com.example.utils.Wait;
import io.cucumber.datatable.DataTable;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.waits.WaitUntil;

import java.util.List;
import java.util.Map;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isPresent;

public class NewMeet implements Task {
    DataTable data;

    public NewMeet(DataTable data) {
        this.data = data;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        List<Map<String,String>> a = data.asMaps(String.class,String.class);
        actor.attemptsTo(
                WaitUntil.the(Meet.BTN_MEET,isPresent()).forNoMoreThan(15).seconds(),
                Click.on(Meet.BTN_MEET),
                WaitUntil.the(Meet.B_MEETING,isPresent()).forNoMoreThan(15).seconds(),
                Click.on(Meet.B_MEETING),
                //////////
                WaitUntil.the(Meet.B_NEW_MEET,isPresent()).forNoMoreThan(15).seconds(),
                Click.on(Meet.B_NEW_MEET),
                /////////
                WaitUntil.the(Meet.NAME_MEET,isPresent()).forNoMoreThan(15).seconds(),
                Enter.theValue(a.get(0).get("meet")).into(Meet.NAME_MEET),
                Click.on(Meet.NEW_TYPE),
                WaitUntil.the(Meet.NAME_TYPE,isPresent()).forNoMoreThan(15).seconds(),
                Enter.theValue(a.get(0).get("type")).into(Meet.NAME_TYPE),
                Click.on(Meet.SAVE_TYPE),
                Enter.theValue(a.get(0).get("number")).into(Meet.NUMBER_MEET),

                Click.on(Meet.START_DATE),
                WaitUntil.the(Meet.START_DATE_DAY,isPresent()).forNoMoreThan(15).seconds(),
                Click.on(Meet.START_DATE_DAY),
                Click.on(Meet.START_DATE_HOUR),

                Click.on(Meet.END_DATE),
                WaitUntil.the(Meet.END_DATE_DAY,isPresent()).forNoMoreThan(15).seconds(),
                Click.on(Meet.END_DATE_DAY),
                Click.on(Meet.END_DATE_HOUR),

                Click.on(Meet.LOCATION),
                WaitUntil.the(Meet.OPTION_LOCATION,isPresent()).forNoMoreThan(15).seconds(),
                Click.on(Meet.OPTION_LOCATION),
                Click.on(Meet.UNIT),
                WaitUntil.the(Meet.OPTION_UNIT,isPresent()).forNoMoreThan(15).seconds(),
                Click.on(Meet.OPTION_UNIT),
                Click.on(Meet.ORGANIZED),
                WaitUntil.the(Meet.OPTION_ORGANIZED,isPresent()).forNoMoreThan(15).seconds(),
                Click.on(Meet.OPTION_ORGANIZED),
                Click.on(Meet.REPORTER),
                WaitUntil.the(Meet.OPTION_REPORTER,isPresent()).forNoMoreThan(15).seconds(),
                Click.on(Meet.OPTION_REPORTER),
                Click.on(Meet.ATTENDEE_MEET),
                WaitUntil.the(Meet.OPTION_ATTENDEE,isPresent()).forNoMoreThan(15).seconds(),
                Click.on(Meet.OPTION_ATTENDEE),

                Click.on(Meet.SAVE_MEET)
        );
        Wait.Time(2000);
    }
    public static NewMeet FillData(DataTable data){
        return Tasks.instrumented(NewMeet.class, data);
    }
}



