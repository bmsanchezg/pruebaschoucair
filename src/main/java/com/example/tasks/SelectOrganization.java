package com.example.tasks;

import com.example.ui.Log;
import com.example.ui.MenuAdmin;
import com.example.utils.Wait;
import io.cucumber.datatable.DataTable;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.waits.WaitUntil;

import java.util.List;
import java.util.Map;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isPresent;

public class SelectOrganization implements Task {
    DataTable data;

    public SelectOrganization(DataTable data) {
        this.data = data;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        List<Map<String,String>> a = data.asMaps(String.class,String.class);
        actor.attemptsTo(
                WaitUntil.the(MenuAdmin.B_ORGANIZATION,isPresent()).forNoMoreThan(15).seconds(),
                Click.on(MenuAdmin.B_ORGANIZATION),
                WaitUntil.the(MenuAdmin.B_BUSSINES,isPresent()).forNoMoreThan(15).seconds(),
                Click.on(MenuAdmin.B_BUSSINES),
                //////////
                WaitUntil.the(MenuAdmin.B_NEW_BUSS,isPresent()).forNoMoreThan(15).seconds(),
                Click.on(MenuAdmin.B_NEW_BUSS),
                //////////
                WaitUntil.the(MenuAdmin.NAME_BUSS,isPresent()).forNoMoreThan(15).seconds(),
                Enter.theValue(a.get(0).get("name_buss")).into(MenuAdmin.NAME_BUSS),
                Click.on(MenuAdmin.PARENT_UNIT),
                WaitUntil.the(MenuAdmin.OPTION_UNIT,isPresent()).forNoMoreThan(15).seconds(),
                Click.on(MenuAdmin.OPTION_UNIT),
                Click.on(MenuAdmin.BTN_SAVE)


        );
    }
    public static SelectOrganization FillData(DataTable data){
        return Tasks.instrumented(SelectOrganization.class, data);
    }
}



