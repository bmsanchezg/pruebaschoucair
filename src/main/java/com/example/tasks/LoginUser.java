package com.example.tasks;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isPresent;

import com.example.ui.Log;
import io.cucumber.datatable.DataTable;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.waits.WaitUntil;

import java.util.List;
import java.util.Map;

public class LoginUser implements Task {
    DataTable data;

    public LoginUser(DataTable data) {
        this.data = data;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        List<Map<String,String>> a = data.asMaps(String.class,String.class);
        actor.attemptsTo(
                WaitUntil.the(Log.ADMIN,isPresent()).forNoMoreThan(15).seconds(),
                Enter.theValue(a.get(0).get("user")).into(Log.ADMIN),
                Enter.theValue(a.get(0).get("password")).into(Log.PASS),
                Click.on(Log.BUTTON_SING)
        );

    }
    public static LoginUser FillData(DataTable data){
        return Tasks.instrumented(LoginUser.class, data);
    }
}



