package com.example.Questions;

import com.example.ui.Validations;
import net.serenitybdd.core.pages.WithByLocator;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;

public class NewBusiness implements Question<String>{

    @Override
    public String answeredBy(Actor actor) {
        return BrowseTheWeb.as(actor).find((WithByLocator) Validations.BUSINESS).getText();
    }
}
