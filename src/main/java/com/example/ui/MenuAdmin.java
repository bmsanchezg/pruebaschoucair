package com.example.ui;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class MenuAdmin {

    public static final Target B_ORGANIZATION = Target.the("Button Organization").located(By.xpath("//span[text()='Organization']"));
    public static final Target B_BUSSINES = Target.the("Button Organization").located(By.xpath("//span[text()='Business Units']"));
    public static final Target B_NEW_BUSS = Target.the("New Buusiness").located(By.xpath("//span[text()=' New Business Unit']"));
    public static final Target NAME_BUSS = Target.the("Name Buusiness").located(By.xpath("//input[@id='Serenity_Pro_Organization_BusinessUnitDialog3_Name']"));
    public static final Target PARENT_UNIT = Target.the("Parent Unit").located(By.xpath("//span[text()='--select--']"));
    public static final Target OPTION_UNIT = Target.the("Option Unit").located(By.xpath("//li[@class='select2-results-dept-0 select2-result select2-result-selectable'][3]"));
    public static final Target BTN_SAVE = Target.the("Button Save").located(By.xpath("//div[@class='tool-button save-and-close-button icon-tool-button']"));

}
