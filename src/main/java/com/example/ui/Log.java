package com.example.ui;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class Log {

    public static final Target ADMIN = Target.the("User Admin").located(By.xpath("//input[@id='StartSharp_Membership_LoginPanel0_Username']"));
    public static final Target PASS = Target.the("Pass").located(By.xpath("//input[@id='StartSharp_Membership_LoginPanel0_Password']"));
    public static final Target BUTTON_SING = Target.the("Sing In").located(By.xpath("//button[@id='StartSharp_Membership_LoginPanel0_LoginButton']"));
}