package com.example.ui;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class Validations {

    public static final Target BUSINESS = Target.the("Business").located(By.xpath("//div[@class='ui-widget-content slick-row odd']/div[@class='slick-cell l1 r1']/a[text()='Business Brayan']"));
    public static final Target MEETING = Target.the("Meeting").located(By.xpath("//div[@CLASS='ui-widget-content slick-row even']/div[@class='slick-cell l3 r3'][text()='Reu Prueba']"));
}