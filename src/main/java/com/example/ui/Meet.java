package com.example.ui;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class Meet {


    public static final Target BTN_MEET = Target.the("Button Meet").located(By.xpath("//span[text()='Meeting']"));
    public static final Target B_MEETING = Target.the("Button Meeting").located(By.xpath("//span[text()='Meetings']"));
    public static final Target B_NEW_MEET = Target.the("Button Meeting").located(By.xpath("//span[text()=' New Meeting']"));

    public static final Target NAME_MEET = Target.the("Name Meet").located(By.xpath("//input[@id='Serenity_Pro_Meeting_MeetingDialog10_MeetingName']"));

    public static final Target NEW_TYPE = Target.the("New Type Meet").located(By.xpath("//a[@title='Edit']"));
    public static final Target NAME_TYPE = Target.the("Name Type Meet").located(By.xpath("//input[@name='Name']"));
    public static final Target SAVE_TYPE = Target.the("Save Name Type Meet").located(By.xpath("//div[@role='dialog']//div[@class='buttons-inner']//span[text()=' Save']"));

    public static final Target NUMBER_MEET = Target.the("Number Meet").located(By.xpath("//input[@id='Serenity_Pro_Meeting_MeetingDialog10_MeetingNumber']"));

    public static final Target START_DATE = Target.the("Start Date Meet").located(By.xpath("//div[@class='field StartDate col-sm-6']//i[@class='fa fa-calendar']"));
    public static final Target START_DATE_DAY = Target.the("Day Star Meet").located(By.xpath("//div[@id='ui-datepicker-div']//a[text()='24']"));
    public static final Target START_DATE_HOUR = Target.the("Hour Star Meet").located(By.xpath("//div[@class='field StartDate col-sm-6']//select[@class='editor s-DateTimeEditor time']/option[@value='08:00']"));

    public static final Target END_DATE = Target.the("End Date Meet").located(By.xpath("//div[@class='field EndDate col-sm-6']//i[@class='fa fa-calendar']"));
    public static final Target END_DATE_DAY = Target.the("Day End Meet").located(By.xpath("//div[@id='ui-datepicker-div']//a[text()='25']"));
    public static final Target END_DATE_HOUR = Target.the("Hour End Meet").located(By.xpath("//div[@class='field EndDate col-sm-6']//select[@class='editor s-DateTimeEditor time']/option[@value='15:00']"));

    public static final Target LOCATION = Target.the("Location Meet").located(By.xpath("//div[@id='s2id_Serenity_Pro_Meeting_MeetingDialog10_LocationId']"));
    public static final Target OPTION_LOCATION = Target.the("Option Location Meet").located(By.xpath("//div[@class='select2-result-label'][text()='On Site']"));

    public static final Target UNIT = Target.the("Unit Meet").located(By.xpath("//div[@id='s2id_Serenity_Pro_Meeting_MeetingDialog10_UnitId']"));
    public static final Target OPTION_UNIT = Target.the("Option Unit Meet").located(By.xpath("//div[@id='select2-result-label-21']"));

    public static final Target ORGANIZED = Target.the("Organized Meet").located(By.xpath("//div[@id='s2id_Serenity_Pro_Meeting_MeetingDialog10_OrganizerContactId']"));
    public static final Target OPTION_ORGANIZED = Target.the("Option Organized Meet").located(By.xpath("//div[@class='select2-result-label'][text()='Adam Stewart']"));

    public static final Target REPORTER = Target.the("Reporter Meet").located(By.xpath("//div[@id='s2id_Serenity_Pro_Meeting_MeetingDialog10_ReporterContactId']"));
    public static final Target OPTION_REPORTER = Target.the("Option Reporter Meet").located(By.xpath("//div[@class='select2-result-label'][text()='Alexis Lopez']"));

    public static final Target ATTENDEE_MEET = Target.the("Ateendee Meet").located(By.xpath("//div[@id='s2id_autogen11']"));
    public static final Target OPTION_ATTENDEE = Target.the("Option Attendee Meet").located(By.xpath("//div[@class='select2-result-label'][text()='Allison Bell']"));

    public static final Target SAVE_MEET = Target.the("Save Meet").located(By.xpath("//div[@class='tool-button save-and-close-button icon-tool-button']"));
}
