Feature: The user wants to fill the form in the UTest page
  @smoke
  Scenario Outline: Fill form
    Given The user has been sent to the Test page
    When The user login the test page
      | user        | password   |
      | <user>      | <password> |
    When The user create the new Business
      | name_buss        |
      | <name_buss>      |
    When The user create one Meet
      | meet        | number | type  |
      | <meet>      |<number>| <type>|

    Examples:
      | user       | password | name_buss            | meet             | number | type      |
      | admin      | serenity | Business Brayan      | Capacitacion     | 12     | Reu Prueba|