package com.example.stepDefinitions;

import com.example.Questions.NewBusiness;
import com.example.tasks.*;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class StartSharpStepDefinition {
    @Before
    public void setTheStage(){
        OnStage.setTheStage(new OnlineCast());
    }
    @Given("The user has been sent to the Test page")
    public void The_user_go_to_page(){
        theActorCalled("user").attemptsTo(GoTo.TheURL());
    }
    @When("The user login the test page")
    public void The_user_login_the_test_page(DataTable data){
        theActorInTheSpotlight().attemptsTo(LoginUser.FillData(data));
    }
    @When("The user create the new Business")
    public void The_user_create_the_new_Business(DataTable data){
        theActorInTheSpotlight().attemptsTo(SelectOrganization.FillData(data));
    }
    //The Business is created
    @When("The user create one Meet")
    public void The_user_create_one_Meet(DataTable data){
        theActorInTheSpotlight().attemptsTo(NewMeet.FillData(data));
    }

}